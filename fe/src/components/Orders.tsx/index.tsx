import { Order } from '../../types/Order';
import OrdersBoard from '../OrdersBoard.tsx';
import { Container } from './styles';

const orders: Order[] = [
  {
    '_id': '63756dd924b16c7c837a75e4',
    'table': '123',
    'status': 'WAITING',
    'products': [
      {
        'product': {
          'name': 'Pizza quatro queijos',
          'imagePath': '1668638199579-quatro-queijos.png',
          'price': 40,
        },
        'quantity': 2,
        '_id': '63756dd924b16c7c837a75e5'
      },
      {
        'product': {
          'name': 'Coca cola',
          'imagePath': '1668639040226-coca-cola.png',
          'price': 35,
        },
        'quantity': 1,
        '_id': '63756dd924b16c7c837a75e6'
      }
    ],
  },
  {
    '_id': '63756dd924b16c7c837wqwa75e4',
    'table': '4678',
    'status': 'WAITING',
    'products': [
      {
        'product': {
          'name': 'Pizza quatro queijos',
          'imagePath': '1668638199579-quatro-queijos.png',
          'price': 40,
        },
        'quantity': 2,
        '_id': '63756dd924b16c7c837a75e5'
      },
    ],
  }
];

export default function Orders() {
  return (
    <Container>
      <OrdersBoard
        icon='🕑'
        title='Fila de espera'
        orders={orders}
      />
      <OrdersBoard
        icon='🧑‍🍳'
        title='Em preparação'
        orders={[]}
      />
      <OrdersBoard
        icon='✅'
        title='Pronto!'
        orders={[]}
      />

    </Container>
  );
}

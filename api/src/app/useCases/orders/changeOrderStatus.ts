import { Order } from './../../models/Order';

import { Request, Response } from 'express';

export async function changeOrderStatus(req: Request, res: Response) {

  try {

    const { orderId } = req.params;
    const { status } = req.body;

    if(!['WAITING','IN_PRODUCTION','DONE'].includes(status)){
      res.status(400).json({
        error: 'Status should be one of these: WAITING, IN_PRODUCTION, DONE.'
      });
    }

    await Order.findByIdAndUpdate(orderId,{ status: status });

    res.sendStatus(204);
  } catch (error) {
    console.error(error);
    res.sendStatus(500);
  }
}

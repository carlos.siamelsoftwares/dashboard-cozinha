import { Order } from './../../models/Order';
import { Request, Response } from 'express';

export async function listOrders(req: Request, res: Response) {

  try {
    const orders = await Order.find()
      .sort({ created_at: 'desc' })
      .populate('products.product');
    res.json(orders);
  }
  catch (error) {
    console.error(error);
    res.sendStatus(500);
  }

}
